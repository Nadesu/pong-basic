﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundBehaviour : MonoBehaviour
{
    public AudioSource Music;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (Music)
            {
                if(Music.isPlaying)
                {
                    Music.Stop();
                }
                Music.Play();
            }
            
        }
    }
}
